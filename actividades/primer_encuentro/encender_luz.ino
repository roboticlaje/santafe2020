// La funcion setup se ejecuta una vez cuando se energiza la placa o se resetea
void setup() {
  // Inicializacion de la luz LED de la placa arduino nano en pin 13
  pinMode(13, OUTPUT);
}

// La funcion loop se ejecuta todo el tiempo mientras se tenga energía
void loop() {
  digitalWrite(13, HIGH);   // se enciende la luz LED
}